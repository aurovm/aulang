# Design

Notes on the design decisions and problems in Auro.

## Similarity with C

Auro aims to be as close as possible to C. Wherever the semantics of Auro can match the semantics of C, the syntax will match as well.

### Memory model

C is designed to be executed in Von Neuman style CPU architectures. That is not the execution model of Auro, so wherever needed, the syntax of Auro will be purposely different to reflect the difference in execution models. Every C construct related to memory is completely removed from Aulang, as Auro doesn't have addressable memory.

Auro does expose addressable memory operations in it's libraries, but that's not the main way Auro is designed to be used, it's only there for programs that need to interact with the host memory. Adding those as builtin syntax would complicate it significantly for the sake of a very limited set of use cases. A C implementation on Auro would be more appropriate to expose those operations in the language syntax.

### Record vs Struct vs Class

Auro records are very similar to C structs: structural record types, ie. collection of named fields where the type matches any other record with the same field types and order (naming doesn't matter).

Although similar, `record` was chosen as the keyword to declare them over `struct` for two reasons:
1. it reflects the name in Auro library specs
2. C structs are passed by value. That means, when passed as an argument, if function receiving the record change any of the fields, the parent function will see the changes, contrary to C where those changes would only be visible in the inner function.

In that respect, auro records behave more similarly to Java classes, but that would also be inaccurate as Auro has no inheritance (also Java classes are nominal).

C-Struct-like semantics can be emulated by never mutating a record, and instead reassign the variable holding the record with modified copies. Efficient compilers should be able to optimize these cases. This strategy could also be used in a proper C implementation on Auro.

## Item expressions

Aulang should make it easy to reason about and manipulate items (modules, types and functions), so syntactic expressions could evaluate to an item, not only to runtime values.

This way, module application is functor construction, module member access is item access, type member access is method access.

Technically (at the moment), item expressions are a subset of runtime expressions. Most runtime operations can't make sense with items.


## Ternary vs null types (Unsolved)

Nullable types are expressed as `T?`. This is simple, convenient and makes sense. But this syntax limits the use of the `?` operator. For null types, `?` is parsed as a postfix operator with no arguments, but for the ternary operator `?` is followed by an expression. Disambiguating between both cases requires ending the parsing of an arbitratily long expression to see if there's a `:` token.

An alternative is to use rust style `if` expressions, but this has two disadvantages: it's ambiguous with the if statement (slightly different syntax definition, requiring statement branches rather than expressions), but more importantly, it's not the same syntax as C uses. Auro should keep C syntax as much as possible.

Possibly, the solution to parsing ternary operator is to assume it's ternary unless an expression terminator follows (`;)}`,  binary operators and assignments), or only parse ternary if the next token is an expression starter.

# Null

Expression terminators: `=;)}`
Null guard: `expr?`
expr: `expr`

```
expr =
    literal
    (expr)
    (expr) expr
    expr OP expr
    expr.name
    expr(expr,+)
    expr ? !( ident ^[;=}]$ )
    expr ? expr : expr
    expr { CONSTRUCTOR }

int? b;
b? // null guard expression
if (b?) ;

a?.b;
a?(b)

a ? (b) : c;
a ? (b) : c;
```

# Automatic Imports

import statements bring items from other modules into scope without them needing to be declared and defined in the file.

Types that are not explicitly exported in an imported module are matched against all type declarations available. If none matches a particular then an anonymous type is created for it.

If an imported type matches more than one local type declaration, then values received of that type must be explicitly casted to one of the matching local types.
