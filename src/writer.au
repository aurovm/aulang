/*
Writer is the class used to create binary buffers.
Those buffers are then written to disk in main.
*/

module buffer_mod = import auro.buffer;
type buffer = buffer_mod.buffer;
buffer newbuf (int size) = buffer_mod. `new`;
int bufsize (buffer) = buffer_mod. size;
int bufget (buffer, int index) = buffer_mod. get;
void bufset (buffer, int index, int value) = buffer_mod. set;

module string_mod = import auro.string;
int strlen (string) = string_mod.length;
buffer strbuf (string) = string_mod.tobuffer;

private int MAX () { return 64*8; }


type PartShell (Part);

record Part {
  buffer buf;
  int size;
  PartShell? _next;
}

extend Part {
  Part? next (Part this) {
    if (this._next is null)
      return null Part;
    return Part?.new(this._next.get() as Part);
  }

  // TODO: Why are these necessary???
  void setSize (Part this, int size) { this.size = size; }
  void setNext (Part this, Part next) {
    this._next = PartShell?.new(PartShell{next});
  }
}

Part newPart () { return Part{ newbuf(MAX()), 0, null PartShell }; }

module intbit = import auro.int.bit;
int shr (int, int) = intbit.shr;
int and (int, int) = intbit.and;
int or (int, int) = intbit.or;

record Writer {
  Part first;
  Part last;
  int count;
}

extend Writer {
  Writer create () {
    Part part = newPart();
    return Writer{part, part, 0};
  }

  void byte (Writer this, int n) {
    Part part = this.last;
    if (part.size >= MAX()) {
      Part next = newPart();
      part.setNext(next);
      //part._next = (next as PartShell) as PartShell?;

      // TODO: Part has no member last$set
      // Why is Auro inferring this to be a Part?????

      //this.last = next;
      //this.count = this.count + 1;
      Writer.last$set(this, next);
      Writer.count$set(this, Writer.count$get(this) + 1);

      part = next;
    }
    bufset(part.buf, part.size, n);
    // part.size = part.size + 1;
    part.setSize(part.size + 1);
  }

  void _num (Writer this, int n) {
    if (n > 127) this._num(shr(n, 7));
    this.byte(or(and(n, 127), 128));
  }

  void num (Writer this, int n) {
    if (n > 127) this._num(shr(n, 7));
    this.byte(and(n, 127));
  }

  void bytes (Writer this, buffer buf) {
    int i = 0;
    while (i < bufsize(buf)) {
      this.byte(bufget(buf, i));
      i = i+1;
    }
  }

  void rawstr (Writer this, string s) {
    this.bytes(strbuf(s));
  }

  void str (Writer this, string s) {
    this.num(strlen(s));
    this.rawstr(s);
  }

  buffer tobuffer (Writer this) {
    buffer buf = newbuf((this.count * MAX()) + this.last.size);
    Part part = this.first;
    int i = 0;
    loop:
      int j = 0;
      while (j < part.size) {
        bufset(buf, i, bufget(part.buf, j));
        j = j+1;
        i = i+1;
      }
      Part? next = part.next();
      if (!next is null) {
        part = next.get();
        goto loop;
      }
    return buf;
  }
}
