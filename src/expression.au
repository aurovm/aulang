/*
Defines entities for partially unresolved expressions.
*/

module system = import auro.system;
void error (string) = system.error;

import aulang.v3.identifier.Identifier;
import aulang.v3.entity.Entity;

module _list = (import utils.arraylist)(module { `0` = Entity; });
type List = _list.``;
List ListNew () = _list.`new`;
int ListLen (List) = _list.len;
Entity ListGet (List, int) = _list.get;
void ListAdd (List, Entity) = _list.push;
extend List {
    List empty () { return ListNew(); }
    int len (List this) { return ListLen(this); }
    Entity get (List this, int i) { return ListGet(this, i); }
    void add (List this, Entity val) { ListAdd(this, val); }

    bool matches (List this, List that) {
        int len = this.len();
        if (!(len == that.len())) return false;

        int i = 0;
        while (i < len) {
            if (!Entity.matches(this.get(i), that.get(i))) return false;
            i = i+1;
        }

        return true;
    }
}

private record _Call {
    Entity fn;
    List args;
}

type Call (_Call);
extend Call {
    Call `new` (Entity fn, List args) {
        return Call{_Call{fn, args}};
    }
    Call oneArg (Entity fn, Entity arg) {
        List args = ListNew();
        args.add(arg);
        return Call{_Call{fn, args}};
    }
    Entity fn$get (Call this) {
        return (this as _Call).fn;
    }
    List args$get (Call this) {
        return (this as _Call).args;
    }
}

private record _Logic {
    string op;
    Entity left;
    Entity right;
}

type Logic (_Logic);
extend Logic {
    Logic `new` (string op, Entity l, Entity r) {
        return Logic{_Logic{op, l, r}};
    }
    string op$get (Logic this) {
        return (this as _Logic).op;
    }
    Entity left$get (Logic this) {
        return (this as _Logic).left;
    }
    Entity right$get (Logic this) {
        return (this as _Logic).right;
    }
}

private record _Access {
    Entity base;
    string name;
}

type Access (_Access);
extend Access {
    Access `new` (Entity base, string name) {
        return Access{_Access{base, name}};
    }
    Entity base$get (Access this) {
        return (this as _Access).base;
    }
    string name$get (Access this) {
        return (this as _Access).name;
    }
}

type Import (string);
extend Import {
    Import `new` (string name) {
        return Import{name};
    }

    string name (Import this) {
        return this as string;
    }
}

record BuildEntry {
    // int or Identifier
    any key;
    Entity value;
    // false or MEntry
    any next;
}

private type _Build (bool);
record Build {
    Entity base;
    any entries;
    // Number of indexed items in this build.
    int count;
    _Build _;
}

Build BuildNew (Entity base) {
    return Build.create(base);
}

extend Build {
    Build create (Entity base) {
        return Build{base, any{false}, 0, _Build{false}};
    }

    void addNamed (Build this, Identifier name, Entity value) {
        this.entries = any{BuildEntry{any{name}, value, this.entries}};
    }

    void addIndexed (Build this, Entity value) {
        int index = this.count;
        this.entries = any{BuildEntry{any{index}, value, this.entries}};
        this.count = index + 1;
    }

    Entity findNamed (Build this, Identifier name) {
        BuildIter iter = this.iter();
        while (!iter.isEmpty()) {
            any key; Entity it;
            key, it, iter = iter.deconstruct();
            if (key is Identifier) {
                Identifier iname = key as Identifier;
                if (iname.matches(name)) return it;
            }
        }
        return Entity.empty();
    }

    int getIndexed (Build this, int index) {
        // TODO
    }

    bool matches (Build this, Entity query) {
        if (!(query.value is Build)) return false;

        any copy = any{false};
        BuildIter iter = this.iter();
        while (!iter.isEmpty()) {
            any key; Entity it;
            key, it, iter = iter.deconstruct();
            copy = any{BuildEntry{key, it, copy}};
        }

        iter = Build.iter(query.value as Build);

        // Go over each entry in query
        while (!iter.isEmpty()) {
            any key; Entity it;
            key, it, iter = iter.deconstruct();

            // Go over each entry in copy, keeping the previous iteration
            any prev = any{false};
            any next = copy;
            while (next is BuildEntry) {
                BuildEntry entry = next as BuildEntry;
                next = entry.next;

                // Check if the keys match
                bool intMatch =
                    key is int && entry.key is int &&
                    (key as int == entry.key as int);
                bool identMatch =
                    key is Identifier && entry.key is Identifier &&
                    Identifier.matches(entry.key as Identifier, key as Identifier);
                if (intMatch || identMatch) {
                    // Remove the entry from the copied list
                    if (prev is bool) {
                        // First item of the list, so replace the entire list
                        copy = entry.next;
                    } else {
                        (prev as BuildEntry).next = entry.next;
                    }
                    goto _continue;
                } else {
                    prev = any{entry};
                }
            }

            // If it reaches this point, the query's entry didn't have a matching copy's entry.
            // That means the builds don't match
            return false;
            _continue:
        }

        // The build matches if the copy is empty after the loop
        bool isEmpty = copy is bool;
        return isEmpty;
    }

    BuildIter iter (Build this) {
        return BuildIter{this.entries};
    }
}

// false or BuildEntry
record BuildIter { any val; }
extend BuildIter {
    bool isEmpty (BuildIter this) {
        return this.val is bool;
    }

    any, Entity, BuildIter deconstruct (BuildIter this) {
        BuildEntry entry = this.val as BuildEntry;
        return entry.key, entry.value, BuildIter{entry.next};
    }
}

private type _Define (bool);
record DefineItem {
    string name;
    Entity value;
    // DefineItem or false
    any next;
}
record Define {
    // DefineItem or false
    any items;
    _Define _;
}
extend Define {
    Define empty () { return Define{any{false}, _Define{false}}; }
    void add (Define this, string name, Entity value) {
        this.items = any{DefineItem{name, value, this.items}};
    }
    any iter (Define this) { return this.items; }
    bool hasNext (any iter) { return iter is DefineItem; }
    string, Entity, any next(any iter) {
        DefineItem item = iter as DefineItem;
        return item.name, item.value, item.next;
    }
}

private type _Builder (bool);
record Builder {
    string kind;
    Entity base;
    _Builder _;
}
extend Builder {
    Builder create (string kind, Entity base) {
        return Builder{kind, base, _Builder{false}};
    }
}