/*
Output holds the items that will be written to the file.
*/

module system = import auro.system;
void error (string) = system.error;

module buffer_mod = import auro.buffer;
type buffer = buffer_mod.buffer;

module string_mod = import auro.string;
int strlen (string) = string_mod.length;

import aulang.v3.items.Module;
import aulang.v3.items.Type;
import aulang.v3.items.Function;

import aulang.v3.writer.Writer;

module _ModuleList = (import utils.arraylist)({ `0` = Module; });
type ModuleList = _ModuleList.``;
ModuleList ModuleListNew () = _ModuleList.`new`;
int ModuleListLen (ModuleList) = _ModuleList.len;
Module ModuleListGet (ModuleList, int) = _ModuleList.get;
void ModuleListPush (ModuleList, Module) = _ModuleList.push;
void ModuleListInsert (ModuleList, int, Module) = _ModuleList.insert;
void ModuleListRemove (ModuleList, int) = _ModuleList.remove;

module _TypeList = (import utils.arraylist)({ `0` = Type; });
type TypeList = _TypeList.``;
TypeList TypeListNew () = _TypeList.`new`;
int TypeListLen (TypeList) = _TypeList.len;
Type TypeListGet (TypeList, int) = _TypeList.get;
void TypeListPush (TypeList, Type) = _TypeList.push;

module _FunctionList = (import utils.arraylist)({ `0` = Function; });
type FunctionList = _FunctionList.``;
FunctionList FunctionListNew () = _FunctionList.`new`;
int FunctionListLen (FunctionList) = _FunctionList.len;
Function FunctionListGet (FunctionList, int) = _FunctionList.get;
void FunctionListSet (FunctionList, int, Function) = _FunctionList.set;
void FunctionListPush (FunctionList, Function) = _FunctionList.push;

module _NodeList = (import utils.arraylist)({ `0` = MetaNode; });
type NodeList = _NodeList.``;
NodeList NodeListNew () = _NodeList.`new`;
int NodeListLen (NodeList) = _NodeList.len;
MetaNode NodeListGet (NodeList, int) = _NodeList.get;
void NodeListSet (NodeList, int, MetaNode) = _NodeList.set;
void NodeListPush (NodeList, MetaNode) = _NodeList.push;

private record _Output {
    ModuleList modules;
    TypeList types;
    FunctionList functions;
    MetaNode metadata;
    MetaNode sourcemap;
    int constants;
}

type MetaNode (any);
type MetaNode {
    MetaNode Str (string s) {
        return s as any as MetaNode;
    }
    MetaNode Num (int n) {
        return n as any as MetaNode;
    }
    MetaNode Tp (Type tp) {
        return tp as any as MetaNode;
    }
    MetaNode Fn (Function fn) {
        return fn as any as MetaNode;
    }
    MetaNode empty () {
        return NodeListNew() as any as MetaNode;
    }

    string kind (MetaNode this) {
        any a = this as any;
        if (a is string) return "s";
        if (a is int) return "n";
        if (a is NodeList) return "l";
    }

    string str (MetaNode this) {
        return this as any as string;
    }
    int num (MetaNode this) {
        return this as any as int;
    }
    NodeList list (MetaNode this) {
        return this as any as NodeList;
    }

    int len (MetaNode this, MetaNode child) {
        NodeList l = this.list();
        return NodeListLen(l);
    }
    void add (MetaNode this, MetaNode child) {
        NodeList l = this.list();
        NodeListPush(l, child);
    }
    MetaNode child (MetaNode this, int i) {
        NodeList l = this.list();
        return NodeListGet(l, i);
    }

    void write (MetaNode this, Writer w) {
        any a = this as any;
        if (a is NodeList) {
            NodeList l = a as NodeList;
            itn len = NodeListLen(l);
            w.num(len * 4);

            int i = 0;
            while (i < len) {
                MetaNode.write(NodeListGet(l, i), w);
                i = i+1;
            }
        } else if (a is string) {
            string s = a as string;
            int len = strlen(s);
            w.num(len * 4 + 2);
            w.rawstr(s);
        } else {
            int n;
            if (a is int) {
                n = a as int;  
            } else if (a is Type) {
                n = Type.getId(a as Type);
            } else if (a is Function) {
                Function f = a as Function;
                n = f.id;
            }
            w.num(n * 2 + 1);
        }
    }
}

type Output (_Output) {
    Output `new` () {
        MetaNode sourcemap = MetaNode.empty();
        sourcemap.add(MetaNode.Str("source map"));

        MetaNode meta = MetaNode.empty();
        meta.add(sourcemap);

        return new _Output(
            ModuleListNew(),
            TypeListNew(),
            FunctionListNew(),
            meta,
            sourcemap,
            0
        ) as Output;
    }
    void addModule (Output this, Module it) { ModuleListPush((this as _Output).modules, it); }
    void addType (Output this, Type it) { TypeListPush((this as _Output).types, it); }
    void addFunction (Output this, Function it) { FunctionListPush((this as _Output).functions, it); }
    void addConstant (Output this, Function it) { FunctionListPush((this as _Output).functions, it); }

    void addSourceMap (Output _this, MetaNode node) {
        _Output this = _this as _Output;
        this.sourcemap.add(node);
    }

    void setExport (Output _this, Module mod) {
        _Output this = _this as _Output;

        int len = ModuleListLen(this.modules);
        int j = 0;
        while (j < len) {
            Module jmod = ModuleListGet(this.modules, j);
            if (jmod.equals(mod)) {
                ModuleListRemove(this.modules, j);
                break;
            }
            j = j+1;
        }

        ModuleListInsert(this.modules, 0, mod);
    }

    void assignIds (Output _this) {
        _Output this = _this as _Output;

        // How many invisible modules have been processed,
        // or by how much the id should be higher than the index
        // (Module 0 is invisible)
        int skipCount = 1;
        int i = 0;
        while (i < ModuleListLen(this.modules)) {
            Module mod = ModuleListGet(this.modules, i);
            mod.id = i+skipCount;

            i = i+1;
            if (mod.isFunctor()) {
                // Skip argument module, its implicit and not present in the output list
                skipCount = skipCount+1;
            }
        }

        int i = 0;
        while (i < TypeListLen(this.types)) {
            TypeListGet(this.types, i).id = i;
            i = i+1;
        }

        int i = 0;
        while (i < FunctionListLen(this.functions)) {
            FunctionListGet(this.functions, i).id = i;
            i = i+1;
        }
    }

    // TODO: sortItems is more accurate
    void sortFunctions (Output _this) {
        _Output this = _this as _Output;

        int len = FunctionListLen(this.functions);

        int i = 0;
        while (i < len - this.constants) {
            Function f = FunctionListGet(this.functions, i);

            if (f.isConstant()) {
                int last = len - this.constants - 1;
                FunctionListSet(this.functions, i, FunctionListGet(this.functions, last));
                FunctionListSet(this.functions, last, f);

                this.constants = this.constants + 1;
            } else {
                i = i+1;
            }
        }

        int len = ModuleListLen(this.modules);

        int i = 0;
        while (i < len) {
            Module mod = ModuleListGet(this.modules, i);
            if (mod.isFunctor()) {
                // Move the exported module next to the functor that exports it
                Module exported = mod.getFunctorExport();

                int exportedIndex = -1;
                int j = 0;
                while (j < len) {
                    Module jmod = ModuleListGet(this.modules, j);
                    if (jmod.equals(exported)) {
                        exportedIndex = j;
                        break;
                    }
                    j = j+1;
                }

                if (exportedIndex >= 0) {
                    if (j == i) error("Functor module #" + itos(i) + " exports itself");
                    if (j < i) {
                        // Exported module before functor
                        // Remove the exported, which will move the functor index back by one
                        ModuleListRemove(this.modules, j);
                        // Insert the export where the functor used to be (that index is not in front)
                        ModuleListInsert(this.modules, i, exported);
                        // Do not increment index (the exported module goes next iteration)
                    } else {
                        // Exported module after functor.
                        // Move it to the next entry
                        ModuleListRemove(this.modules, j);
                        ModuleListInsert(this.modules, i+1, exported);
                        // Don't skip next index (the exported module, could be a functor itself)
                        i = i+1;
                    }

                } else {
                    // Exported not found in output list.
                    // Just add it without concern
                    ModuleListInsert(this.modules, i+1, exported);
                    i = i+1;
                }
            } else {
                i = i+1;
            }
        }
    }

    void print (Output _this) {
        _Output this = _this as _Output;

        println("Modules:");
        int i = 0;
        while (i < ModuleListLen(this.modules)) {
            Module mod = ModuleListGet(this.modules, i);
            println("    [" + itos(mod.id) + "] " + mod.toOutputString());
            i = i+1;
        }

        println("Types:");
        int i = 0;
        while (i < TypeListLen(this.types)) {
            Type tp = TypeListGet(this.types, i);
            println("    [" + itos(tp.id) + "] " + tp.toOutputString());
            i = i+1;
        }

        println("Functions:");
        int i = 0;
        while (i < FunctionListLen(this.functions)) {
            Function fn = FunctionListGet(this.functions, i);
            println("    [" + itos(fn.id) + "] " + fn.toOutputString());
            i = i+1;
        }
    }

    buffer write (Output _this) {
        _Output this = _this as _Output;

        Writer w = Writer.create();

        w.rawstr("Auro 0.6");
        w.byte(0);

        int count = ModuleListLen(this.modules);
        w.num(count);
        int i = 0;
        while (i < count) {
            Module it = ModuleListGet(this.modules, i);
            it.writeOutput(w);
            i = i+1;
        }

        int count = TypeListLen(this.types);
        w.num(count);
        int i = 0;
        while (i < count) {
            Type it = TypeListGet(this.types, i);
            it.writeOutput(w);
            i = i+1;
        }


        int count = FunctionListLen(this.functions) - this.constants;

        w.num(count);
        int i = 0;
        while (i < count) {
            Function it = FunctionListGet(this.functions, i);
            it.writeOutput(w);
            i = i+1;
        }

        w.num(this.constants);
        int i = count;
        int count = FunctionListLen(this.functions);
        while (i < count) {
            Function it = FunctionListGet(this.functions, i);
            it.writeConstant(w);
            i = i+1;
        }

        int i = 0;
        while (i < count) {
            Function it = FunctionListGet(this.functions, i);
            it.writeCode(w);
            i = i+1;
        }

        // Metadata
        this.metadata.write(w);

        return w.tobuffer();
    }
}