/*
Defines the (fully resolved) item entities: Module, Type and Function.

Partially resolved items (like generics, or lazily imported items) are not defined here.
*/

module system_mod = import auro.system;
module buffer_mod = import auro.buffer;
type buffer = buffer_mod.buffer;
void error (string) = system_mod.error;
int bufsize (buffer) = buffer_mod.size;

import aulang.v3.writer.Writer;
import aulang.v3.`import`.items.Type as IType;
import aulang.v3.`import`.items.Expression as IExpression;
import aulang.v3.`import`.items.Get as IGet;
import aulang.v3.`import`.items.Import as IImport;

import aulang.v3.dict.Dict;

module _TypeList = (import utils.arraylist)({ `0` = Type; });
type TypeList = _TypeList.``;
TypeList TypeListNew () = _TypeList.`new`;
int TypeListLen (TypeList) = _TypeList.len;
Type TypeListGet (TypeList, int) = _TypeList.get;
void TypeListPush (TypeList, Type) = _TypeList.push;
type TypeList {
    int len (TypeList this) { return TypeListLen(this); }
    Type get (TypeList this, int index) { return TypeListGet(this, index); }
    void push (TypeList this, Type tp) { TypeListPush(this, tp); }
}

module _FunctionList = (import utils.arraylist)({ `0` = Function; });
type FunctionList = _FunctionList.``;
FunctionList FunctionListNew () = _FunctionList.`new`;
int FunctionListLen (FunctionList) = _FunctionList.len;
Function FunctionListGet (FunctionList, int) = _FunctionList.get;
void FunctionListPush (FunctionList, Function) = _FunctionList.push;
type FunctionList {
    int len (FunctionList this) { return FunctionListLen(this); }
    Function get (FunctionList this, int index) { return FunctionListGet(this, index); }
    void push (FunctionList this, Function tp) { FunctionListPush(this, tp); }
}

module _PairList = (import utils.arraylist)({ `0` = Pair; });
type PairList = _PairList.``;
PairList PairListNew () = _PairList.`new`;
int PairListLen (PairList) = _PairList.len;
Pair PairListGet (PairList, int) = _PairList.get;
void PairListPush (PairList, Pair) = _PairList.push;
type PairList {
    int len (PairList this) { return PairListLen(this); }
    Pair get (PairList this, int index) { return PairListGet(this, index); }
    void push (PairList this, Pair tp) { PairListPush(this, tp); }
}

module _code = import aulang.v3.code;
type Code = _code.Code;
Code CodeNew () = _code.`new`$Code;
string CodeSerialize (Code) = _code.serialize$Code;
void CodeWrite (Code, Writer) = _code.write$Code;

module _Map = (import utils.stringmap)({ `0` = any; });
type Map = _Map.``;
Map MapNew () = _Map.`new`;
any? MapGet (Map, string) = _Map.get;
void MapSet (Map, string, any) = _Map.set;
type Map {
    any? get (Map this, string key) { return MapGet(this, key); }
    void set (Map this, string key, any val) { MapSet(this, key, val); }
}


private record _Item {
    Module mod;
    string name;
}

type Item (_Item) {
    Item `new` (Module mod, string name) {
        return new _Item(mod, name) as Item;
    }

    Module mod$get (Item _this) { return (_this as _Item).mod; }
    string name$get (Item _this) { return (_this as _Item).name; }

    string toItemString (Item _this) {
        _Item this = _this as _Item;
        return this.mod.toItemString() + "." + this.name;
    }
    void write (Item _this, Writer w, int offset) {
        _Item this = _this as _Item;
        w.num(this.mod.getId() + offset);
        w.str(this.name);
    }
    bool fitsImport (Item _this, any imp) {
        _Item this = _this as _Item;
        if (imp is IGet) {
            IGet iget = imp as IGet;
            return iget.name == this.name &&
                this.mod.fitsImport(iget.base);
        }
        return false;
    }
    bool equals (Item _this, Item _that) {
        _Item this = _this as _Item;
        _Item that = _that as _Item;
        return this.mod.equals(that.mod) && this.name == that.name;
    }
}

private record Pair {
    string name;
    any item;
}

type ModuleDefine (PairList) {
    ModuleDefine empty () { return PairListNew() as ModuleDefine; }
    void add (ModuleDefine this, string name, any item) {
        PairList pl = this as PairList;
        pl.push(new Pair(name, item));
    }
}

private record ModuleBuild {
    Module mod;
    Module arg;
}

private record _Module {
    int id;
    any data;
    Dict items;
    bool _eq;
}

type Module (_Module);
private type ModuleImport (string);
private type ModuleAlias (Module);

private type ModuleFunctor (Module);
private type ModuleFunctorArg (Module);

private Module newModule (any data) { return new _Module(-1, data, Dict.empty(), false) as Module; }


type Module {
    Module empty () { return newModule(false as any); }
    Module _import (string name) { return newModule(name as ModuleImport as any); }
    Module use (Item item) { return newModule(item as any); }

    void makeUse (Module this, Item item) { (this as _Module).data = item as any; }
    void makeImport (Module this, string name) { (this as _Module).data = name as ModuleImport as any; }
    void makeBuild (Module this, Module base, Module arg) {
        (this as _Module).data = new ModuleBuild(base, arg) as any;
    }
    void makeAlias (Module this, Module alias) { (this as _Module).data = alias as ModuleAlias as any; }
    void makeDefine (Module this, ModuleDefine def) { (this as _Module).data = def as any; }

    Module getFunctorArgument (Module this) { return newModule(this as ModuleFunctorArg as any); }
    void setFunctorExport (Module this, Module exp) { (this as _Module).data = exp as ModuleFunctor as any; }

    bool isFunctor (Module this) { return (this as _Module).data is ModuleFunctor; }
    Module getFunctorExport (Module this) { return (this as _Module).data as ModuleFunctor as Module; }

    bool equals (Module _this, Module _that) {
        _Module this = _this as _Module;
        _Module that = _that as _Module;
        this._eq = true;
        bool b = that._eq;
        this._eq = false;
        return b;
    }

    int getId (Module _this) {
        start:
        _Module this = _this as _Module;
        if (this.data is ModuleFunctorArg) {
            Module base = this.data as ModuleFunctorArg as Module;
            return base.getId() + 1;
        }
        if (this.data is ModuleAlias) {
            _this = this.data as ModuleAlias as Module;
            goto start;
        }
        return this.id;
    }

    Dict items$get (Module _this) { return (_this as _Module).items; }

    int id$get (Module _this) { return _this.getId(); }
    void id$set (Module this, int id) { (this as _Module).id = id; }

    Module unalias (Module this) {
        start:
        any data = (this as _Module).data;
        if (data is ModuleAlias) {
            this = data as ModuleAlias as Module;
            goto start;
        }
        return this;
    }

    string toItemString (Module _this) {
        int id = _this.getId();
        string sid;
        if (id >= 0) {
            sid = itos(id);
        } else {
            sid = "?";
        }
        return "modules[" + sid + "]";
    }

    string toOutputString (Module _this) {
        _Module this = _this as _Module;
        if (this.data is bool && !(this.data as bool)) {
            return "NULL";
        } else if (this.data is ModuleImport) {
            return "import " + this.data as ModuleImport as string;
        } else if (this.data is ModuleDefine) {
            return "DEFINE";
        } else if (this.data is Item) {
            Item data = this.data as Item;
            return data.toItemString();
        } else if (this.data is ModuleBuild) {
            ModuleBuild build = this.data as ModuleBuild;
            return build.mod.toItemString() + "(" + build.arg.toItemString() + ")";
        } else if (this.data is ModuleAlias) {
            Module al = this.data as ModuleAlias as Module;
            return al.toOutputString();
        } else {
            return "INVALID MODULE";
        }
    }

    void writeOutput (Module _this, Writer w) {
        _Module this = _this as _Module;
        if (this.data is bool && !(this.data as bool)) {
            w.byte(0);
        } else if (this.data is ModuleImport) {
            w.byte(1);
            w.str(this.data as ModuleImport as string);
        } else if (this.data is ModuleDefine) {
            PairList pairs = this.data as ModuleDefine as PairList;
            w.byte(2);
            w.num(pairs.len());

            int i = 0;
            while (i < pairs.len()) {
                Pair p = pairs.get(i);
                any item = p.item;
                int k, id;
                if (item is Module) {
                    Module m = item as Module;
                    k = 0; id = m.getId();
                } else if (item is Type) {
                    Type t = item as Type;
                    k = 1; id = t.getId();
                } else if (item is Function) {
                    Function f = item as Function as _Function;
                    k = 2; id = f.id;
                } else {
                    error("invalid define item");
                }
                w.byte(k);
                w.num(id);
                w.str(p.name);
                i = i+1;
            }
        } else if (this.data is Item) {
            w.byte(3);
            Item data = this.data as Item;
            data.write(w, 0);
        } else if (this.data is ModuleBuild) {
            ModuleBuild build = this.data as ModuleBuild;
            w.byte(4);
            w.num(build.mod.getId());
            w.num(build.arg.getId());
        } else if (this.data is ModuleFunctor) {
            w.byte(5);
        } else if (this.data is ModuleFunctorArg) {
            // It's not written in the output
        } else {
            error("Invalid module");
        }
    }

    bool fitsImport (Module _this, any imp) {
        _Module this = _this as _Module;
        if (imp is IImport && this.data is ModuleImport) {
            return this.data as ModuleImport as string == IImport.get(imp as IImport);
        }
        return false;
    }
}

record Field {
    Function get;
    Function set;
}

record Builder {
    Function fn;
    Map fields; // Map$<int>
}

private record _Type {
    int id;
    any def;
    string name;

    Map fields; // Map$<Field>
    Map methods; // Map$<Function>
    Builder? builder;

    // used internally to prove equality
    bool _compared;
}

type Type (_Type);
private type TypeAlias (Type);


type Type {
    Type empty () { return new _Type(
        -1, false as any, "",
        MapNew(), MapNew(), null Builder,
        false
    ) as Type; }

    Type item (Item def) { Type t = Type.empty(); t.makeItem(def); return t; }

    void makeItem (Type this, Item def) { (this as _Type).def = def as any; }
    void makeAlias (Type this, Type alias) { (this as _Type).def = alias as TypeAlias as any; }

    int getId (Type _this) {
        start:
        _Type this = _this as _Type;
        if (this.def is TypeAlias) {
            _this = this.def as TypeAlias as Type;
            goto start;
        }
        return this.id;
    }

    int id$get (Type _this) { return _this.getId(); }
    void id$set (Type this, int id) { (this as _Type).id = id; }

    string name$get (Type this) { return (this.unalias() as _Type).name; }
    void name$set (Type this, string id) { (this as _Type).name = id; }

    Type unalias (Type this) {
        start:
        any def = (this as _Type).def;
        if (def is TypeAlias) {
            this = def as TypeAlias as Type;
            goto start;
        }
        return this;
    }

    void builderCreate (Type _this, Function fn) {
        _Type this = _this as _Type;
        this.builder = new Builder(fn, MapNew()) as Builder?;
    }

    void builderAddField (Type _this, string name, int index) {
        _Type this = _this as _Type;
        Builder b = this.builder as Builder;
        b.fields.set(itos(index), name as any);
    }

    void addField (Type _this, string name, Function get, Function set) {
        _Type this = _this as _Type;
        this.fields.set(name, new Field(get, set) as any);
    }

    Function? getBuilderFunction (Type _this) {
        _Type this = _this as _Type;
        if (this.builder is null) {
            return null Function;
        } else {
            Builder b = this.builder as Builder;
            return b.fn as Function?;
        }
    }

    string getBuilderFieldName (Type _this, int index) {
        _Type this = _this as _Type;
        Builder b = this.builder as Builder;
        any? r = b.fields.get(itos(index));
        return r as any as string;
    }

    Function? getFieldGetter (Type _this, string name) {
        _Type this = _this as _Type;
        any? f = this.fields.get(name);
        if (f is null) return null Function;
        else return (f as any as Field).get as Function?;
    }

    Function? getFieldSetter (Type _this, string name) {
        _Type this = _this as _Type;
        any? f = this.fields.get(name);
        if (f is null) return null Function;
        else return (f as any as Field).set as Function?;
    }

    // We abuse the mutability of Type to detect if two types are the same instance
    bool eq (Type _a, Type _b) {
        _Type a = _a.unalias() as _Type;
        _Type b = _b.unalias() as _Type;
        a._compared = true;
        bool r = b._compared;
        a._compared = false;
        return r;
    }

    string toItemString (Type _this) {
        int id = _this.getId();
        string sid;
        if (id >= 0) {
            sid = itos(id);
        } else {
            sid = "?";
        }
        return "types[" + sid + "]";
    }

    string toOutputString (Type _this) {
        _Type this = _this as _Type;
        if (this.def is bool) {
            return "NULL";
        } else if (this.def is Item) {
            Item def = this.def as Item;
            return def.toItemString();
        } /*else if (this.def is TypeAlias) {
            TypeAlias alias = this.def as TypeAlias;
            return (alias as Type).toItemString();
        }*/ else {
            return "INVALID TYPE";
        }
    }

    void writeOutput (Type _this, Writer w) {
        _Type this = _this as _Type;
        if (this.def is bool) {
            w.byte(0);
        } else if (this.def is Item) {
            Item def = this.def as Item;
            def.write(w, 1);
        } /*else if (this.def is TypeAlias) {
            TypeAlias alias = this.def as TypeAlias;
            return (alias as Type).toItemString();
        }*/ else {
            error("INVALID");
        }
    }

    bool fitsImport (Type _this, any _imp) {
        if (!(_imp is IType)) return false;
        IType imp = _imp as IType;

        _Type this = _this as _Type;
        if (this.def is Item) {
            any idef = imp.def.value();
            return Item.fitsImport(this.def as Item, idef);
        }

        return false;
    }
}


private record _Function {
    int id;
    TypeList ins;
    TypeList outs;
    any data;
    bool _eq;
}
type Function (_Function);
private type FunctionAlias (Function);

private type BytesConst (buffer);
private type IntConst (int);

private record _CallConst {
    Function fn;
    FunctionList args;
}

type CallConst (_CallConst) {
    void addArg (CallConst _this, Function arg) {
        // This is not resolving as a one liner. I don't know why.
        _CallConst this = _this as _CallConst;
        this.args.push(arg);
    }
}

private int getFunctionId (Function _this) {
    start:
    _Function this = _this as _Function;
    if (this.data is FunctionAlias) {
        _this = this.data as FunctionAlias as Function;
        goto start;
    }
    return this.id;
}

type Function {
    Function empty () { return new _Function(-1, TypeListNew(), TypeListNew(), false as any, false) as Function; }
    Function item (Item def) {
        Function f = Function.empty();
        f.makeItem(def);
        return f;
    }

    void makeItem (Function this, Item def) { (this as _Function).data = def as any; }
    void makeAlias (Function this, Function alias) { (this as _Function).data = alias as FunctionAlias as any; }
    void makeInt (Function this, int n) { (this as _Function).data = n as IntConst as any; }
    void makeBytes (Function this, buffer buf) { (this as _Function).data = buf as BytesConst as any; }
    Code makeCode (Function this) {
        Code code = CodeNew();
        (this as _Function).data = code as any;
        return code;
    }

    CallConst makeCall (Function this, Function call) {
        CallConst data = new _CallConst(call, FunctionListNew()) as CallConst;
        (this as _Function).data = data as any;
        return data;
    }

    int id$get (Function this) { return (this as _Function).id; }
    void id$set (Function this, int id) { (this as _Function).id = id; }

    void addInput (Function _this, Type tp) {
        _Function this = _this as _Function;
        this.ins.push(tp);
    }

    void addOutput (Function _this, Type tp) {
        _Function this = _this as _Function;
        this.outs.push(tp);
    }

    int inCount (Function _this) { _Function this = _this as _Function; return this.ins.len(); }
    int outCount (Function _this) { _Function this = _this as _Function; return this.outs.len(); }

    Type getInput (Function _this, int i) { _Function this = _this as _Function; return this.ins.get(i); }
    Type getOutput (Function _this, int i) { _Function this = _this as _Function; return this.outs.get(i); }

    string toItemString (Function _this) {
        int id = getFunctionId(_this);
        string sid;
        if (id >= 0) {
            sid = itos(id);
        } else {
            sid = "?";
        }
        return "functions[" + sid + "]";
    }

    bool isConstant (Function _this) {
        _Function this = _this as _Function;
        any d = this.data;
        return d is IntConst || d is BytesConst || d is CallConst;
    }

    string toOutputString (Function _this) {
        _Function this = _this as _Function;
        string r = "(";

        TypeList ins = this.ins;
        TypeList outs = this.outs;

        int i = 0;
        while (i < ins.len()) {
            if (i > 0) r = r + ", ";
            Type t = ins.get(i);
            r = r + t.toItemString();
            i = i+1;
        }

        r = r + ") -> (";

        int i = 0;
        while (i < outs.len()) {
            if (i > 0) r = r + ", ";
            Type t = outs.get(i);
            r = r + t.toItemString();
            i = i+1;
        }

        r = r + ") ";

        string body;
        if (this.data is bool) {
            body = "NULL";
        } else if (this.data is Item) {
            Item data = this.data as Item;
            body = data.toItemString();
        } else if (this.data is TypeAlias) {
            body = "ALIAS";
        } else if (this.data is Code) {
            body = CodeSerialize(this.data as Code);
        } else if (this.data is IntConst) {
            body = "int " + itos(this.data as IntConst as int);
        } else if (this.data is BytesConst) {
            //buffer buf = this.data as BytesConst as buffer;
            body = "bytes { ";
            // TODO: Print every byte
            body = body + " }";
        } else if (this.data is CallConst) {
            _CallConst const = this.data as CallConst as _CallConst;
            body = const.fn.toItemString() + "(";

            int i = 0;
            while (i < const.args.len()) {
                if (i > 0) body = body + ", ";
                Function arg = const.args.get(i);
                body = body + arg.toItemString() + "()";
                i = i+1;
            }
            // TODO: Print every byte
            body = body + ")";
        } else {
            body = "INVALID";
        }
        return r + body;
    }

    void writeOutput (Function _this, Writer w) {
        _Function this = _this as _Function;

        if (this.data is bool) {
            w.byte(0);
        } else if (this.data is Item) {
            _Item data = this.data as Item as _Item;
            w.num(data.mod.getId() + 2);
        } else if (this.data is Code) {
            w.byte(1);
        } else {
            error("invalid");
        }

        TypeList ins = this.ins;
        TypeList outs = this.outs;

        w.num(ins.len());
        int i = 0;
        while (i < ins.len()) {
            Type t = ins.get(i);
            w.num(t.getId());
            i = i+1;
        }

        w.num(outs.len());
        int i = 0;
        while (i < outs.len()) {
            Type t = outs.get(i);
            w.num(t.getId());
            i = i+1;
        }

        if (this.data is Item) {
            _Item data = this.data as Item as _Item;
            w.str(data.name);
        }
    }

    void writeConstant (Function _this, Writer w) {
        _Function this = _this as _Function;

        if (this.data is IntConst) {
            w.byte(1);
            w.num(this.data as IntConst as int);
        } else if (this.data is BytesConst) {
            buffer buf = this.data as BytesConst as buffer;
            w.byte(2);
            w.num(bufsize(buf));
            w.bytes(buf);
        } else if (this.data is CallConst) {
            _CallConst const = this.data as CallConst as _CallConst;

            _Function fn = const.fn as _Function;

            if (const.args.len() != fn.ins.len()) {
                error("Argument count mismatch");
            }

            w.num(fn.id + 16);

            int i = 0;
            while (i < const.args.len()) {
                _Function arg = const.args.get(i) as _Function;
                w.num(arg.id);
                i = i+1;
            }
        } else {
            error("invalid");
        }
    }

    void writeCode (Function _this, Writer w) {
        _Function this = _this as _Function;

        if (this.data is Code) {
            CodeWrite(this.data as Code, w);
        }
    }

    bool equals (Function _this, Function _that) {
        _Function this = _this as _Function;
        _Function that = _that as _Function;

        this._eq = true;
        bool r = that._eq;
        this._eq = false;
        return r;
    }
}
