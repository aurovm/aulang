/*
The entry point for the compilation phase (after parsing).

The compiler is split in multiple files. This file also exposes
the main functions necessary for the submodules.

Most of the main procedure is in compiler/entity.au
*/

module system = import auro.system;
module string_mod = import auro.string;
module buffer_mod = import auro.buffer;

type buffer = buffer_mod.buffer;

void exit (int) = system.exit;

import aulang.v3.error.Error;
import aulang.v3.node.Node;
import aulang.v3.span.Span;
import aulang.v3.identifier.Identifier;
import aulang.v3.output.Output;
import aulang.v3.output.MetaNode;
import aulang.v3.state.Compiler;
import aulang.v3.scope.Scope;

import aulang.v3.entity.Entity;

// auro can't import functions (but it can import the modules)
import aulang.v3.util; // readall
import aulang.v3.parser; // parse
import aulang.v3.writer.Writer;
import aulang.v3.global.Global;

module c_entity = import aulang.v3.compiler.entity;
module c_partials = import aulang.v3.compiler.partials;
module c_builtin = import aulang.v3.compiler.builtin;
module c_decl = import aulang.v3.compiler.decl;

type FieldExpression = c_partials.FieldExpression;

void compileRoot(Compiler c) = c_decl.compileRoot;
Entity getConcreteItem (Compiler, Entity) = c_entity.getConcrete;
Entity getRecordGeneric(Compiler c, int fieldCount) = c_builtin.getRecordGeneric;

Entity resolveExpression (Compiler, Scope, Node) = c_entity.resolveExpression;
Entity resolveEntity (Compiler, Scope, Entity) = c_entity.resolveEntity;
Entity resolveFieldGet (Compiler, Scope, FieldExpression, Span) = c_entity.resolveFieldGet;
Entity resolveFieldSet (Compiler, Scope, Entity, Identifier, Entity, Span) = c_entity.resolveFieldSet;
Entity resolveIndexSet (Compiler, Scope, Entity, Entity, Entity, Span) = c_entity.resolveIndexSet;


// TODO: Result
buffer compile_src (string src, string path, Global global) {
    Node root = parser.parse(src);
    Compiler c = Compiler.withRootNode(root, path, global);

    MetaNode fileNode = MetaNode.empty();
    fileNode.add(MetaNode.Str("file"));
    fileNode.add(MetaNode.Str(path));
    c.out.addSourceMap(fileNode);

    compileRoot(c);

    if (c.hasError()) {
        c.printErrors();
        //c.out.print();
        exit(1);
    }

    c.out.sortFunctions();
    c.out.assignIds();
    // c.out.print();

    return c.out.write();
}
