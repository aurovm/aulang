/*
TODO: Document
*/

import aulang.v3.entity.Entity;
import aulang.v3.span.Span;
import aulang.v3.items.Function;
import aulang.v3.writer.Writer;

module _RegList = (import utils.arraylist)(module{ `0` = Reg; });
type RegList = _RegList.``;
RegList RegListNew () = _RegList.`new`;
int RegListLen (RegList) = _RegList.len;
Reg RegListGet (RegList, int) = _RegList.get;
void RegListPush (RegList, Reg) = _RegList.push;
extend RegList {
    int len (RegList this) { return RegListLen(this); }
    Reg get (RegList this, int index) { return RegListGet(this, index); }
    void push (RegList this, Reg tp) { RegListPush(this, tp); }
}

module _InstList = (import utils.arraylist)(module{ `0` = Inst; });
type InstList = _InstList.``;
InstList InstListNew () = _InstList.`new`;
int InstListLen (InstList) = _InstList.len;
Inst InstListGet (InstList, int) = _InstList.get;
void InstListPush (InstList, Inst) = _InstList.push;
extend InstList {
    int len (InstList this) { return InstListLen(this); }
    Inst get (InstList this, int index) { return InstListGet(this, index); }
    void push (InstList this, Inst tp) { InstListPush(this, tp); }
}

module _IntMap = (import utils.stringmap)(module{ `0` = int; });
type IntMap = _IntMap.``;
IntMap IntMapNew() = _IntMap.`new`;
int? IntMapGet (IntMap, string) = _IntMap.get;
void IntMapSet (IntMap, string, int) = _IntMap.set;

// register id/location
record _Reg {
    int id;
    Entity tp;
    bool tmp;
}

type Reg (_Reg);
extend Reg {
    int id (Reg this) {
        _Reg r = this as _Reg;
        return r.id;
    }

    Entity tp (Reg this) {
        _Reg r = this as _Reg;
        return r.tp;
    }

    bool isTmp (Reg this) {
        _Reg r = this as _Reg;
        return r.tmp;
    }

    void setType (Reg this, Entity tp) {
        (this as _Reg).tp = tp;
    }
}

// label id (not location)
type Label (string);


private record _Inst {
    int kind;
    RegList args;
    Label? lbl;
    Function? fn;
}

type Inst (_Inst);
extend Inst {
    Inst create (int code) {
        return Inst{_Inst{code, RegListNew(), null Label, null Function}};
    }

    Inst jmp (int code, Label lbl) {
        return Inst{_Inst{code, RegListNew(), Label?.new(lbl), null Function}};
    }

    Inst call (Function fn) {
        return Inst{_Inst{16, RegListNew(), null Label, Function?.new(fn)}};
    }

    void addArg (Inst _this, Reg arg) {
        _Inst this = _this as _Inst;
        this.args.push(arg);
    }

    string toString (Inst _this) {
        _Inst this = _this as _Inst;

        if (this.kind == 16) {
            Function fn = this.fn.get();
            string r = fn.toItemString() + "(";

            int i = 0;
            while (i < this.args.len()) {
                if (i > 0) r = r + ", ";
                Reg arg = this.args.get(i);
                r = r + "reg_" + itos(arg.id());
                i = i+1;
            }

            return r + ")";
        } else {
            string r = itos(this.kind);
            if (!(this.lbl is null)) {
                r = r + " lbl_" + this.lbl.get() as string;
            }
            if (this.args.len() > 0) {
                int i = 0;
                while (i < this.args.len()) {
                    Reg arg = this.args.get(i);
                    r = r + "reg_" + itos(arg.id());
                    i = i+1;
                }
            }
            return r;
        }
    }

    void write (Inst _this, Writer w, _Code code) {
        _Inst this = _this as _Inst;

        if (this.kind == 16) {
            Function fn = this.fn.get();
            w.num(fn.id + 16);
        } else {
            w.num(this.kind);
            if (!(this.lbl is null)) {
                w.num(IntMapGet(code.labels, this.lbl.get() as string).get());
            }
        }

        int i = 0;
        while (i < this.args.len()) {
            Reg arg = this.args.get(i);
            w.num(arg.id());
            i = i+1;
        }
    }
}

private record _Code {
    InstList instructions;
    int regcount;
    int lblcount;
    IntMap labels;
}

type Code (_Code);
extend Code {
    Code `new` () { return Code{_Code{InstListNew(), 0, 0, IntMapNew()}}; }

    void addInst (Code _this, Inst inst) {
        _Code this = _this as _Code;
        this.instructions.push(inst);
    }

    Reg reg (Code _this, Entity tp) {
        _Code this = _this as _Code;
        int count = this.regcount;
        this.regcount = count + 1;
        return Reg{_Reg{count, tp, true}};
    }

    Reg var (Code _this, Entity tp) {
        _Code this = _this as _Code;
        int count = this.regcount;
        this.regcount = count + 1;
        return Reg{_Reg{count, tp, false}};
    }

    Label declLabel (Code _this) {
        _Code this = _this as _Code;
        int i = this.lblcount;
        this.lblcount = i+1;
        return Label{"I" + itos(i)};
    }

    Label namedLabel (Code _this, string name) {
        return Label{name};
    }

    void addLabel (Code _this, Label lbl) {
        _Code this = _this as _Code;
        IntMapSet(this.labels, lbl as string, _this.count());
    }

    int count (Code _this) {
        _Code this = _this as _Code;
        return this.instructions.len();
    }

    string serialize (Code _this) {
        string prefix = "\n    ";

        _Code this = _this as _Code;

        int len = this.instructions.len();
        if (len == 0) return "{ 0 instructions }";

        string lines = "{";

        string _p = prefix + "    ";

        lines = lines + _p + "[ " + itos(len) + " instructions ]";
        lines = lines + _p + "[ " + itos(this.regcount) + " registers ]";

        int i = 0;
        while (i < len) {
            Inst inst = this.instructions.get(i);
            lines = lines + _p + inst.toString();
            i = i+1;
        }
        return lines + prefix + "}";
    }

    void write (Code _this, Writer w) {
        _Code this = _this as _Code;

        int len = this.instructions.len();
        w.num(len);

        int i = 0;
        while (i < len) {
            Inst inst = this.instructions.get(i);
            inst.write(w, this);
            i = i+1;
        }
    }
}