/*
Node is a weakly typed, string centric representation of an AST node.
In the future I hope to make proper ADT Ast nodes, when Aulang has better
support for such an idiom.
*/

module nodearr = import auro.utils.arraylist(module { `0` = Node; });
type NodeArr = nodearr.``;
NodeArr NodeArrNew () = nodearr.`new`;
Node NodeArrGet (NodeArr, int) = nodearr.get;
void NodeArrPush (NodeArr, Node) = nodearr.push;
int NodeArrLen (NodeArr) = nodearr.len;

import aulang.v3.span.Span;

record Node {
  string tp;
  string val;
  Span span;
  NodeArr children;
}

extend Node {
  Node create (string tp, string val) {
    return Node.new(tp, val, Span.empty(), NodeArrNew());
  }

  NodeArr children (Node this) { return this.children; }

  int len (Node this) {
    return NodeArrLen(this.children());
  }

  Node child (Node this, int index) {
    return NodeArrGet(this.children(), index);
  }

  void push (Node this, Node child) {
    NodeArrPush(this.children(), child);
  }

  int line$get (Node this) {
    return this.span.line;
  }

  void line$set (Node this, int line) {
    this.span = Span.create(line, 0);
  }

  void print (Node this, string indent) {
    string pos = "";
    if (this.span.line > 0) { pos = "[" + itos(this.span.line) + "] "; }

    println(indent + pos + this.tp + " " + this.val);
    int i = 0;
    while (i < this.len()) {
      Node child = this.child(i);
      child.print(indent + "  ");
      i = i+1;
    }
  }

  string to_string (Node this) {
    string s = this.tp;
    if (!(this.val == "")) {
      s = s + ":" + this.val;
    }
    if (this.len() > 0) {
      s = s + "[";
      int i = 0;
      while (i < this.len()) {
        if (i > 0) s = s + ", ";
        Node child = this.child(i);
        s = s + child.to_string();
        i = i+1;
      }
      s = s + "]";
    }
    return s;
  }

  Node addSpan (Node this, Span span) {
    this.span = this.span.join(span);
    return this;
  }

  Node inline (Node this, int line) {
    this.span = Span.create(line, 0);
    return this;
  }
}