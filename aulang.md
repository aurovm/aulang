# Auro language (Aulang)

*[2018-01-09 16:45]*

The Auro language is a thin layer on top of the Auro module format similar to C.

# Lexical

Lexically Aulang is very similar to C, with the following remarks:

- There is no pointer related syntax
- A quoted identifier is like a string but delimited with \` instead of " and has the same meaning as a regular identifier
- The set of reserved keywords is different than C's:
  + true, false, void, null, new, if, else, while, return, continue, break, goto, type, module, import, export, as
- Default types are not keywords but implicitly imported types:
  + bool, int, float, string
- There is no octal integer literal
- String escape sequences are different than C's

Note: the code snippets are marked as C to take advantage of markdown syntax highlighters, but they're actually Auro code.

# Top Level Statements

~~~c
// Global modules
module system = import auro.system;
// Member modules of other modules
module unsafe = system.unsafe;
// Module definitions
module int_param {
  T = int;
}
// Parametric modules
module int_arr_mod = array(intparam);

// Types can only be members of other modules
type int_arr = int_arr_mod.array;

// functions can be either from another module
void println (string msg) = system.println;
// Or defined here
void main () {}
// Or not defined at all (useful for documentation or platform
// specific functions throught metadata)
void main ();

// Any non ternary nor logic expression is valid
const int my_const = 40 + 2;

export mymodule;

// Item name syntax is allowed when importing items from modules and when
// defining items. $ is replaced with the metaname separator 0x1d.
int arr$get = 0;
~~~

~~~c
// You can define record types (structs in C) with the record keyword.
record coord {
    int x;
    int y;
}

// In the background, it does
module _coord_mod = (import auro.record)({
    `0` = int; `1` = int;
});
type coord = _coord_mod;
int _get_x (coord) = _coord_mod.get0;
int _get_y (coord) = _coord_mod.get1;

// One detail with this is that all the structs with the same field types in the same order
// are interchangeable with each other.



// You can define methods on a type
extend coord {
    coord zero () {
        return coord{x=0, y=0};
    }

    int distanceSquared (coord this) {
        return this.x * this.x + this.y * this.y;
    }

    // You can define custom fields by using the get set name convention.
    // The compiler also picks up on imported functions like this.
    int z$get (coord this) { return 0; }
}

// If the methods are public, it will export them by using the method name,
// with its owner type name appended.



// A type wrapper is a completely different type. Instances of the wrapper type
// cannot be used interchangeably with the base type, even tho they use the same value.

// TODO: At the moment, the inner value field syntax is not yet implemented.
//type point (coord coord);
type point (coord);

void testPoint () {
    coord c = coord{x=1, y=2};
    point p = point{p};

    // This is the target syntax but its not yet implemented
    //coord c2 = p.$;
    //coord c2 = p.coord; // if the inner value is named
    coord c2 = p as coord;
}



// You can define a distinct record type by using type record combination
// TODO: This feature is not yet implemented
type record duration {
    int seconds;
    int millis;
}

~~~

~~~c
module child (argument) {

}
~~~

# Control flow

Control flow is just like C's, but there are no switch, for loops nor do while loops. There are labels and goto statements, and loops can be labeled so that break and continue statements can refer to outer loops. It is legal to jump out of loops and into them.

# Multiple assignment

Multiple assignment in declaration statements works like in C, but multiple variables can be assigned at once in a single statement when the assigned expression is a call to a function with multiple results. In this situation, the left side of the statement can have multiple variables separated by commas, and each one must match the function return types. But it must not be a declaration, as that would only assign the last variable and the rest would be uninitialized, like in C.

# Consts

TODO

# Expressions

Expressions can be unary operations, binary operations and function calls.

Arithmetic and relation operations are overloaded for ints and floats. Addition and equality are also overloaded for strings. Logical operations are short circuited and only work with booleans.

Values aren't casted implicitly, nor is there an explicit cast expression, if casting is desired it must be trough library functions.

Function calls that return multiple values when used in expressions use the first result and discard the rest. Only in an assignment it's possible to use more than one result from a function call.

# Metadata

~~~c
#metadata ("one", 2, (main, string));
~~~

Each metadata directive declares an individual metadata node, which will be added to all the others. A node can be a string, an integer, a parenthezised list of nodes, or a function/type/module name, which when compiled point to their ids.
