#!/bin/bash

# This script cleans a Graphviz .dot file by removing nodes whose labels
# start with "f_" and all edges associated with those nodes.

# Graph lines format:
# Node:   "1234 [label=\"f_abc\", other_attributes]"
# Edge:   "123 -> 456 [attributes]"

# To generate a graph in the first place, use:
# nugget aurodump --dot --dir dist aulang.v3.$MODULE_NAME > $GRAPH_FILE

# Check if a graph file is provided
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <graph.dot>"
    exit 1
fi

GRAPH_FILE="$1"

# Step 1: Find node IDs whose labels start with "f_"
awk -F '[ \\[=]' '/label="f_/ {print $1}' "$GRAPH_FILE" > remove_ids.txt

# Step 2: Remove the nodes and their edges
while read id; do
    sed -i "/^$id\s*\[/d; /^\s*$id\s*->\s*/d" "$GRAPH_FILE"
done < remove_ids.txt