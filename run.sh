#!/bin/env bash

if [ "$1" == "-h" -o "$1" == "--help" -o "$1" == "help" ]; then
  echo "run.sh [ -h | --help | help | test | dist | install | uninstall ]"
  exit
fi

FILES="aulang aulang.util aulang.node aulang.item aulang.lexer aulang.parser aulang.writer aulang.importer aulang.codegen aulang.compiler"

NUGGET_BIN=nugget
if [ -n $NUGGET ]; then
  NUGGET_PATH=$(dirname $(realpath $NUGGET_BIN))
  export LD_LIBRARY_PATH=$NUGGET_PATH:$LD_LIBRARY_PATH
fi

if [ "$AURO" = "" ]; then
  AURO=$NUGGET_BIN
fi

# Este comando remplaza todas las palabras por aulang.palabra
# echo $X | sed -E 's/\w+/aulang.&/g'

aspath () { echo $1 | tr . /; }
compile () { echo compiling $1; auro aulang -L dist src/`aspath $1`.au dist/aulang.v3.$1; }
v3compile () { echo compiling $1; $NUGGET_BIN aulang.v3 -L dist src/`aspath $1`.au dist/aulang.v3.$1; }

function buildall {
  # rm dist/*

  v3compile util &&
  v3compile span &&
  v3compile node &&
  v3compile identifier &&
  v3compile error &&
  v3compile dict &&

  v3compile lexer &&
  v3compile parser &&
  v3compile writer &&

  v3compile entity &&
  v3compile import.items &&
  v3compile import.read &&

  v3compile extension &&
  v3compile scope &&
  compile items &&
  v3compile expression &&
  v3compile decl &&
  v3compile code &&
  compile generic &&

  compile importer &&
  compile output &&
  compile global &&
  compile state &&

  compile compiler &&
  compile compiler.partials &&
  compile compiler.builtin &&
  compile compiler.entity &&
  v3compile compiler.decl &&
  compile compiler.code &&

  compile entityext &&

  echo compiling aulang &&
  auro aulang -L dist src/aulang.au dist/aulang.v3
  true

  if [ $? == 1 ]; then
    echo "Could not compile files"
    exit 1
  fi
}

if [ -z "$1" ]; then
  $NUGGET_BIN --dir dist aulang test.au out && aurodump --no-metadata out
  exit
fi


if [ "$1" == "build" -o "$1" == "b" ]; then
  if [ -z "$2" ]; then
    buildall
  elif [ "$2" == "aulang" ]; then
    echo compiling aulang
    auro aulang -L dist src/aulang.au dist/aulang
  elif [ -f "src/`aspath $2`.au" ]; then
    compile $2
  else
    echo "no source file $2 found"
    exit 1
  fi
  exit
fi

if [ "$1" == "b3" ]; then
  v3compile $2
fi

if [ "$1" == "temp" ]; then
  cd dist; cp $FILES ..; cd ..
fi

if [ "$1" == "bootstrap" ]; then
  name=$2
  file=src/`aspath $name`.au
  $NUGGET_BIN --monitor-namespace aulang.v3 --profile profile.csv --dir dist aulang.v3 -L dist $file out


  #bash build.sh &&
  #(cd dist; cp $FILES ..) &&
  #echo Bootstrapping &&
  #bash build.sh
  #rm -f $FILES
  exit
fi

function test_file {
  RESULT=test.au
  dir=tests
  query=$1

  if [[ $query =~ ^fail/ ]]; then
    dir=tests/fail
    query=${1#"fail/"}
  fi

  # test whether $1 is made of digits
  if [[ $query =~ ^[0-9]+$ ]]; then
    # convert it to a 2 digit, 0-padded string
    N=`printf "%02d" $query`
    RESULT=$dir/${N}_*.au
  elif [[ -n $query ]]; then
    RESULT=$dir/*$query.au
  fi

  # TODO: check only one file matched

  if [ ! -f $RESULT ]; then
    echo $RESULT is not a file
    exit 1
  fi

  echo $RESULT
}

function run_test {
  TEST_FILE=$1

  # if test_file errored, print the messahe
  if [ $? == 1 ]; then
    echo $TEST_FILE
    exit 1
  fi

  DIST=tests/dist
  OUT=$DIST/main

  # Make sure the directory exists and it's empty
  if [ ! -d $DIST ]; then
    mkdir $DIST
  elif [ -n "`ls $DIST`" ]; then
    rm $DIST/*
  fi

  # Compile dependencies, if any
  USES=`cat $TEST_FILE | sed -n 's/.*\/\/@use \(.*\)/\1/p'`
  for DEP in $USES; do
    echo compiling $DEP
    $NUGGET_BIN --dir dist aulang.v3 tests/deps/$DEP.au $DIST/$DEP || exit
  done

  expected_error=`cat $TEST_FILE | sed -n 's/.*\/\/@error \(.*\)/\1/p'`

  # Compile the main file
  echo compiling $TEST_FILE
  compile_out=`$NUGGET_BIN --dir dist aulang.v3 -L $DIST $TEST_FILE $OUT`
  compile_code=$?

  if [[ -n "$expected_error" ]]; then
    if [[ $compile_code == 1 ]]; then
      #diff --color <(echo "$expected_error") <(echo "$compile_out")

      echo Expected:
      echo "$expected_error"
      echo
      echo Output:
      echo "$compile_out"

      exit 0
    else
      echo Compilation should have failed with
      echo "$expected_error"
      exit 1
    fi
  elif [[ $compile_code != 0 ]]; then
    echo Compilation failed
    echo "$compile_out"
    exit 1
  else
    # Print compilation output regardless
    echo "$compile_out"
  fi

  # If no run, only print the resulting module
  if grep -q -F "//@norun" "$TEST_FILE"; then
    aurodump $OUT
  else
    # Collect expected output, if any
    EXPECTED=`cat $TEST_FILE | sed -n 's/.*\/\/@expect \(.*\)/\1/p'`

    RESULT=`$AURO --dir $DIST $OUT`
    code=$?

    if [[ $code != 0 ]]; then
      echo Test Failed
      echo "$RESULT"
      exit 1
    fi

    if [[ -n "$EXPECTED" && "$RESULT" != "$EXPECTED" ]]; then
      echo Test Failed
      #echo "Output:  " $RESULT
      #echo "Expected:" $EXPECTED
      diff --color <(echo "$EXPECTED") <(echo "$RESULT") 
      exit 1
    fi

    echo "Test run succesfully"
  fi
}

if [ -z "$1" ]; then
  $NUGGET_BIN --dir dist aulang test.au out && aurodump out
  exit
fi

if [ "$1" == "test" ]; then

  COUNT=39
  FAIL_COUNT=5

  if [ "$2" == "all" ]; then
    FAILED=""
    for i in $(seq 1 $COUNT); do
        TEST_FILE=`test_file $i`
        OUT=`run_test "$TEST_FILE"`

        if [ $? == 1 ]; then
          FAILED="$FAILED $i"
          echo "F $TEST_FILE"
        else
          echo "- $TEST_FILE"
        fi
    done

    for i in $(seq 1 $FAIL_COUNT); do
        TEST_FILE=`test_file fail/$i`
        OUT=`run_test "$TEST_FILE"`

        if [ $? == 1 ]; then
          FAILED="$FAILED $i"
          echo "F $TEST_FILE"
        else
          echo "- $TEST_FILE"
        fi
    done

    if [ -z "$FAILED" ]; then
      exit 1
    fi
  else
    TEST_FILE=`test_file $2`

    # if test_file errored, print the messahe
    if [ $? == 1 ]; then
      echo $TEST_FILE
      exit 1
    fi

    run_test "$TEST_FILE"
  fi
  exit
fi

if [ "$1" == "uninstall" ]; then
  echo TODO: Uninstall
  exit 1

  for a in $FILES; do
    auro --remove $a
  done
  exit
fi

if [ "$1" == "install" -o "$1" == "install-build" ]; then
  cd dist
  for file in `ls`; do
    auropm -f $file i
  done
  exit
fi

if [ "$1" == "parse" ]; then
  TEST_FILE=`test_file $2`
  if [ $? == 1 ]; then
    echo $TEST_FILE
    exit 1
  fi

  echo Parse $TEST_FILE
  $NUGGET_BIN --dir dist/ aulang.parser "$TEST_FILE"
fi