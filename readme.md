# Aulang

C-like programming language running on the [Auro VM](https://gitlab.com/aurovm/spec#auro). It's main focus is to be a glue language between Auro internals and other higher level languages, but it has multiple high level features that make it productive even for complex applications.

[Language Reference](/aulang.md).

[Online editor](http://arnaud.com.ve/auro/?lang=aulang).

To install it, you first need to install an [Auro VM](https://gitlab.com/aurovm/spec) implementation (at them oment only [Nugget](https://gitlab.com/aurovm/nugget) is complete), and then run `bash build.sh install`.

To compile a auro file, run `auro aulang.v3 <source.au> <target>` and run it with `auro <target>`.


# V2

Aulang has gone through multiple iterations over its development. The current version is **v3**, but because there are many programs still written in **v2**, the newer, previously incomplete v3 was put in a separate namespace.

Now that v3 is robust enough, the namespace can be updated and that's one of the next things in line to do with Aulang.

However, the current version of the compiler is still written partly in v2 so it needs to be installed in order to compile v3. Note that its not necessary to compile aulang in order to install it as the repository includes compiled modules.

To install it, checkout the `v2` branch in the aulang repository and follow its installation instructions.

v2 itself has source files in the older `culang` version, but v2 is unmaintained and there should be no need to recompile the modules in the v2 branch


## Next

At the moment, unless very necessary, new features are on hold while I clean up and orgainze the compiler. But after that's done, the most important things that need work in Aulang are:

- Some compilation errors lack source location
- Some function calls are compiled without source location, which is significant for runtime stack traces
- Some types and functions, particularly imported types, appear unreadable in error messages
- Stabilize any and null syntax
- The compiler doesn't recognize imported types that are declared as generic
- Compile multiple files, seeing each others items

And among planned new front-facing features are:

- Type records (records that are also distinct types)
- Function types and function expressions
- Algebraic Data Types / Enums / Tagged Unions
- Operator overloading
- Ternary operator
- Import lexical declarations
- Array syntax
- for and switch statements
- char type
